if keyboard_check_pressed(vk_up) {
	var wall = instance_place(x, y - moveSpeedY, parWall);
	var interactable = instance_place(x, y - moveSpeedY, parInteractable);
	if interactable != noone {
		damageInteractable(self, interactable);
	} else if wall == noone {
		y -= moveSpeedY;
		camera_set_view_pos(view_camera[0], camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]) - moveSpeedY);
	}
}

if keyboard_check_pressed(vk_down) {
	var wall = instance_place(x, y + moveSpeedY, parWall);
	var interactable = instance_place(x, y + moveSpeedY, parInteractable);
	if interactable != noone {
		damageInteractable(self, interactable);
	} else if wall == noone {
		y += moveSpeedY;
			camera_set_view_pos(view_camera[0], camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]) + moveSpeedY);
	}
}

if keyboard_check_pressed(vk_left) {
	var wall = instance_place(x - moveSpeedX, y, parWall);
	var interactable = instance_place(x - moveSpeedX, y, parInteractable);
	if interactable != noone {
		damageInteractable(self, interactable);
	} else if wall == noone {
		x -= moveSpeedX;
		camera_set_view_pos(view_camera[0], camera_get_view_x(view_camera[0]) - moveSpeedX, camera_get_view_y(view_camera[0]));
	}
}

if keyboard_check_pressed(vk_right) {
	var wall = instance_place(x + moveSpeedX, y, parWall);
	var interactable = instance_place(x + moveSpeedX, y, parInteractable);
	if interactable != noone {
		damageInteractable(self, interactable);
	} else if wall == noone {
		x += moveSpeedX;
		camera_set_view_pos(view_camera[0], camera_get_view_x(view_camera[0]) + moveSpeedX, camera_get_view_y(view_camera[0]));
	}
}

if keyboard_check_pressed(vk_space) {
	// Place ritual line
	instance_create_layer(x, y, "Instances", lineMaterial);
}
