{
    "id": "09bb9cc6-7e3a-4dc7-9c47-b4201aa5db71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parControllable",
    "eventList": [
        {
            "id": "d41f3094-eb9e-419c-a9b1-11501bc2d516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09bb9cc6-7e3a-4dc7-9c47-b4201aa5db71"
        },
        {
            "id": "d4b3da86-c1f4-4537-b8e6-9572acbc3c15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09bb9cc6-7e3a-4dc7-9c47-b4201aa5db71"
        },
        {
            "id": "fa9ab993-4c19-45ad-932a-8890b5239672",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "09bb9cc6-7e3a-4dc7-9c47-b4201aa5db71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2620a618-2689-4628-b477-23fc9991dd8a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}