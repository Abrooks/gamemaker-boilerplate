if timerTrigger > 0 && timer == timerTrigger {
	// textString = "";
	timer = 0;
	timerTrigger = 0;
} else if timerTrigger > 0 {
	draw_set_alpha(textAlpha);
	draw_set_colour(textColour);
	draw_set_halign(textHalign);
	draw_set_valign(textValign);
	draw_text(textX, textY, textString);
	timer++;
}