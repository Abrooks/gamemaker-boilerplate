if interactableHealth < 0 {
	instance_destroy();
}

if timerTrigger > 0 && timer == timerTrigger {
	sprite_index = spriteDefault;
	timer = 0;
	timerTrigger = 0;
} else if timerTrigger > 0 {
	timer++;
}