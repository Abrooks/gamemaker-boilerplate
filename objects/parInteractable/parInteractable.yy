{
    "id": "44f450c3-f39c-41fe-a264-80602af87bae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parInteractable",
    "eventList": [
        {
            "id": "5cfa55f2-63cc-49be-9f95-d297b2fd3d1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44f450c3-f39c-41fe-a264-80602af87bae"
        },
        {
            "id": "5680bf11-3710-488a-a110-d3ab1e1970a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44f450c3-f39c-41fe-a264-80602af87bae"
        },
        {
            "id": "ba30d133-3cce-48bf-a306-cc55709470e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "44f450c3-f39c-41fe-a264-80602af87bae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2620a618-2689-4628-b477-23fc9991dd8a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}