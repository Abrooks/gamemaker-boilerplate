var player = argument0;
var interactable = argument1;

interactable.sprite_index = interactable.spriteDamaged;
interactable.interactableHealth -= player.interactableDamage;
interactable.timerTrigger = 5;