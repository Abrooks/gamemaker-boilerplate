var key = object_get_name(argument0);

if ds_map_exists(oPlayer.items, key) {
	ds_map_replace(oPlayer.items, key, ds_map_find_value(oPlayer.items, key) + argument1);
} else {
	ds_map_add(oPlayer.items, key, argument1);
}